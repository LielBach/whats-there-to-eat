import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#ff6659',
            main: '#d32f2f',
            dark: '#9a0007',
            contrastText: '#ffffff'
        },
        secondary: {
            light: '#ffd95a',
            main: '#f9a825',
            dark: '#c17900',
            contrastText: '#000000'
        }
    },
    typography: {
        useNextVariants: true
    }
});

export default theme;
