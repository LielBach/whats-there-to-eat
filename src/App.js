import React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';

import Router from './Router';
import Theme from './Theme.style';

const App = () => (
    <MuiThemeProvider theme={Theme}>
        <Router />
    </MuiThemeProvider>
);

export default App;
