import { combineReducers } from 'redux';
import { routerReducer } from '@uirouter/redux';

import questionsReducer from './questions/QuestionsReducer';

export default combineReducers({
    router: routerReducer,
    questions: questionsReducer
});
