import {
    GET_QUESTIONS
} from './QuestionsActionsTypes';

const initialState = {
    questions: []
};

export default (state = initialState, action) => {
    switch (action.type) {
    case GET_QUESTIONS:
        state = {
            ...state,
            questions: action.payload
        };
        break;
    default:
    }

    return state;
};
