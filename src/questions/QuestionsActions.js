import { GET_QUESTIONS } from './QuestionsActionsTypes';

export const getQuestions = () =>
    dispatch => dispatch({
        type: GET_QUESTIONS,
        payload: ['Do you want something cold or hot?',
            'Do you want vegetables in it?']
    });
