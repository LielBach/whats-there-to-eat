import React from 'react';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const QuestionsView = ({ questions }) =>
    (
        <Grid
            container
            direction='column'
            style={{
                height: '100vh'
            }}
            justify='center'
            alignItems='center'
        >
            {questions.map(question => (
                <Typography variant="h4" color="inherit" key={question}>
                    {question}
                </Typography>
            ))}
        </Grid>
    );

export default QuestionsView;
