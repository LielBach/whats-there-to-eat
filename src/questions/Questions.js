import React, { Component } from 'react';
import { connect } from 'react-redux';
import QuestionsView from './QuestionsView';
import { getQuestions } from './QuestionsActions';

class Questions extends Component {
    componentDidMount () {
        this.props.getQuestions();
    };

    render () {
        return (
            <QuestionsView questions={this.props.questions} />
        );
    }
};

const mapStateToProps = ({ questions }) =>
    ({ questions: questions.questions });

export default {
    name: 'questions',
    url: '/questions',
    component: connect(mapStateToProps, { getQuestions })(Questions)
};
