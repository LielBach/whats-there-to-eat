import React from 'react';
import { UIRouterReact, UIView, pushStateLocationPlugin } from '@uirouter/react';
import { withTheme } from '@material-ui/core/styles';
import { createRouterMiddleware } from '@uirouter/redux';
import { ConnectedUIRouter } from '@uirouter/redux/lib/react';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore, compose } from 'redux';
import thunk from 'redux-thunk';

import Reducer from './Reducer';
import Intro from './Intro';
import Questions from './questions/Questions';

const uiRouter = new UIRouterReact();
const routerMiddleware = createRouterMiddleware(uiRouter);
const store = createStore(Reducer,
    compose(applyMiddleware(routerMiddleware), applyMiddleware(thunk)));
const defaultState = {
    name: 'default',
    url: '/',
    redirectTo: 'intro'
};

const router = ({ theme }) => {
    const primaryThemeColor = theme.palette.primary;

    return (
        <Provider store={store}>
            <ConnectedUIRouter
                router={uiRouter}
                plugins={[pushStateLocationPlugin]}
                states={[defaultState, Intro, Questions]}>
                <div style={{
                    backgroundColor: primaryThemeColor.main,
                    color: primaryThemeColor.contrastText
                }}>
                    <UIView />
                </div>
            </ConnectedUIRouter>
        </Provider>
    );
};

export default withTheme()(router);
