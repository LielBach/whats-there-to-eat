import React from 'react';
import { UISref } from '@uirouter/react';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const Intro = () =>
    (
        <Grid
            container
            direction='column'
            style={{
                height: '100vh'
            }}
            justify='center'
            alignItems='center'
            spacing={40}
        >
            <Grid item>
                <Typography variant="h4" color="inherit">
                    What`s There To Eat?!
                </Typography>
            </Grid>
            <Grid item>
                <UISref to="questions">
                    <Button variant="contained" color="secondary">
                        <Typography variant="button" color="inherit">
                            Continue
                        </Typography>
                    </Button>
                </UISref>
            </Grid>
        </Grid>
    );

export default {
    name: 'intro',
    url: '/intro',
    component: Intro
};
